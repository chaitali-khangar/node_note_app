// var personString = '{"name": "Chaitali", "age": 25}'
// var person = JSON.parse(personString);
// console.log(typeof person);
const fs = require('fs');
var originalNote = {
  title: 'Some title',
  body: 'Some Body'
}

// original note string
originalNoteString = JSON.stringify(originalNote);

fs.writeFileSync('notes.json',originalNoteString);

var noteString = fs.readFileSync('notes.json');
var note = JSON.parse(noteString);

console.log(note);
