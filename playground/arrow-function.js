var square = x => x * x;
console.log(square(10));

var user = {
  name: 'Chaitali',
  sayHi: () => {
    console.log('Arguments: ', arguments);
    console.log(`Hi ${user.name}`);
  },
  sayHiAlt(){
    console.log('Arguments: ', arguments);
    console.log(`Hi ${this.name}`);
  }
};

user.sayHiAlt(1,2,3);
